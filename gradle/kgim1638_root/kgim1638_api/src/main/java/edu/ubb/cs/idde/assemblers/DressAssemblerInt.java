package edu.ubb.cs.idde.assemblers;

import edu.ubb.cs.idde.DressDTO;
import edu.ubb.cs.idde.models.Dress;

public interface DressAssemblerInt extends AbstractAssembler<Dress, DressDTO> {}
