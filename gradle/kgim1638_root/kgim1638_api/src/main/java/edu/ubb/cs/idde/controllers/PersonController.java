package edu.ubb.cs.idde.controllers;

import edu.ubb.cs.idde.PersonDTO;
import edu.ubb.cs.idde.assemblers.PersonAssembler;
import edu.ubb.cs.idde.models.Person;
import edu.ubb.cs.idde.services.PersonService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "https://pls-dot-wired-rhino-225308.appspot.com/")
//@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/api")
public class PersonController {

    @Autowired
    private PersonService personService;

    @Autowired
    private PersonAssembler personAssembler;

    @RequestMapping(value = "/persons")
    public ResponseEntity<List<PersonDTO>> findAllPersons() throws Exception {
        try {
            List<Person> persons = personService.findAllPersons();
            List<PersonDTO> personDtos =  new ArrayList<PersonDTO>();

            for(Person person:persons)
            {
                personDtos.add(personAssembler.modelToDTO(person));
            }

            return ResponseEntity.ok(personDtos);
        } catch (Exception e) {
            throw new Exception("findAllPersons failed");
        }
    }

    @GetMapping("/persons/{id}")
    public ResponseEntity<PersonDTO> findPersonById(@PathVariable long id) throws Exception {
        PersonDTO personDTO;
        try {
            try {
                personDTO = personAssembler.modelToDTO(personService.findPersonById(id));
                return ResponseEntity.ok(personDTO);
            } catch (NotFoundException e) {
                throw new Exception("person not found");
            }
        } catch (Exception e) {
            throw new Exception("getPersonById failed");
        }
    }

    @PostMapping("/persons")
    public ResponseEntity<String> createPerson(@RequestBody PersonDTO personDTO) throws Exception {
        try {
            personService.savePerson(personAssembler.dtoToModel(personDTO));
            return ResponseEntity.ok("person successful created");
        } catch (Exception e) {
            throw new Exception("create person failed");
        }
    }

    @PutMapping("/persons/{id}")
    public ResponseEntity<Object> updatePerson(@RequestBody PersonDTO personDTO, @PathVariable long id) throws  Exception {
        try {
            personService.updatePerson(personAssembler.dtoToModel(personDTO), id);
            return ResponseEntity.ok("person successful updated");
        } catch(NotFoundException e){
            throw new Exception("person not found");
        }
        catch (Exception e) {
            throw new Exception("createPerson failed");
        }
    }

    @DeleteMapping("/persons/{id}")
    public ResponseEntity<String> deletePerson(@PathVariable long id) throws Exception {
        try {
            personService.deletePersonById(id);
            return ResponseEntity.ok("person successful deleted");
        } catch (Exception e) {
            throw new Exception("deletePersonById failed");
        }
    }
}
