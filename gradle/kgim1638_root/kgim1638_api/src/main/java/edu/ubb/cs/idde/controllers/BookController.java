package edu.ubb.cs.idde.controllers;

import edu.ubb.cs.idde.BookDTO;
import edu.ubb.cs.idde.assemblers.BookAssembler;
import edu.ubb.cs.idde.models.Book;
import edu.ubb.cs.idde.services.BookService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "https://pls-dot-wired-rhino-225308.appspot.com/")
//@CrossOrigin(origins = "http://localhost:3000c")
@RestController
@RequestMapping(value = "/api")
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private BookAssembler bookAssembler;

    @RequestMapping(value = "/books")
    public ResponseEntity<List<BookDTO>> findAllBooks() throws Exception {
        try {
            List<Book> books = bookService.findAllBooks();
            List<BookDTO> bookDtos =  new ArrayList<BookDTO>();

            for(Book book:books)
            {
                bookDtos.add(bookAssembler.modelToDTO(book));
            }

            return ResponseEntity.ok(bookDtos);
        } catch (Exception e) {
            throw new Exception("findAllBooks failed");
        }
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<BookDTO> findBookById(@PathVariable long id) throws Exception {
        BookDTO bookDTO;
        try {
            try {
                bookDTO = bookAssembler.modelToDTO(bookService.findBookById(id));
                return ResponseEntity.ok(bookDTO);
            } catch (NotFoundException e) {
                throw new Exception("book not found");
            }
        } catch (Exception e) {
            throw new Exception("getBookById failed");
        }
    }

    @PostMapping("/books")
    public ResponseEntity<String> createBook(@RequestBody BookDTO bookDTO) throws Exception {
        try {
            bookService.saveBook(bookAssembler.dtoToModel(bookDTO));
            return ResponseEntity.ok("book successful created");
        } catch (Exception e) {
            throw new Exception("create book failed");
        }
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<Object> updateBook(@RequestBody BookDTO bookDTO, @PathVariable long id) throws  Exception {
        try {
            bookService.updateBook(bookAssembler.dtoToModel(bookDTO), id);
            return ResponseEntity.ok("book successful updated");
        } catch(NotFoundException e){
            throw new Exception("book not found");
        }
        catch (Exception e) {
            throw new Exception("createBook failed");
        }
    }

    @DeleteMapping("/books/{id}")
    public ResponseEntity<String> deleteBook(@PathVariable long id) throws Exception {
        try {
            bookService.deleteBookById(id);
            return ResponseEntity.ok("book successful deleted");
        } catch (Exception e) {
            throw new Exception("deleteBookById failed");
        }
    }
}
