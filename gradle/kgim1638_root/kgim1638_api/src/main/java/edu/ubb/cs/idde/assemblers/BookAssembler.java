package edu.ubb.cs.idde.assemblers;

import edu.ubb.cs.idde.BookDTO;
import edu.ubb.cs.idde.models.Book;
import org.springframework.stereotype.Service;

@Service
public class BookAssembler implements BookAssemblerInt{

    @Override
    public BookDTO modelToDTO(Book book) {
        return new BookDTO(book.getId(), book.getTitle(), book.getAuthor());
    }

    @Override
    public Book dtoToModel(BookDTO bookDTO) {
        return new Book(bookDTO.getId(), bookDTO.getTitle(), bookDTO.getAuthor());
    }
}

