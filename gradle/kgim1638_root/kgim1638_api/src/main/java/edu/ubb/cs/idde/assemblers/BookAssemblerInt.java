package edu.ubb.cs.idde.assemblers;

import edu.ubb.cs.idde.BookDTO;
import edu.ubb.cs.idde.models.Book;

public interface BookAssemblerInt extends AbstractAssembler<Book, BookDTO> {}
