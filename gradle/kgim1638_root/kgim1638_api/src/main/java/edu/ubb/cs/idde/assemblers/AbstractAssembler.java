package edu.ubb.cs.idde.assemblers;


import edu.ubb.cs.idde.BaseDTO;
import edu.ubb.cs.idde.models.BaseEntity;

public interface AbstractAssembler<Model extends BaseEntity, Dto extends BaseDTO> {
    Dto modelToDTO(Model model);
    Model dtoToModel(Dto dto);
}
