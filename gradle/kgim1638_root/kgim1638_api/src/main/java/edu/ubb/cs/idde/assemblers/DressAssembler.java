package edu.ubb.cs.idde.assemblers;

import edu.ubb.cs.idde.DressDTO;
import edu.ubb.cs.idde.models.Dress;
import org.springframework.stereotype.Service;

@Service
public class DressAssembler implements DressAssemblerInt{

    @Override
    public DressDTO modelToDTO(Dress dress) {
        return new DressDTO(dress.getId(), dress.getColour(), dress.getSize());
    }

    @Override
    public Dress dtoToModel(DressDTO dressDTO) {
        return new Dress(dressDTO.getId(), dressDTO.getColour(), dressDTO.getSize());
    }
}

