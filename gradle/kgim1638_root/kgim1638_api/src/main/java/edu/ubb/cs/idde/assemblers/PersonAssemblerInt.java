package edu.ubb.cs.idde.assemblers;

import edu.ubb.cs.idde.PersonDTO;
import edu.ubb.cs.idde.models.Person;

public interface PersonAssemblerInt extends AbstractAssembler<Person, PersonDTO> {}
