package edu.ubb.cs.idde.assemblers;

import edu.ubb.cs.idde.PersonDTO;
import edu.ubb.cs.idde.models.Person;
import org.springframework.stereotype.Service;

@Service
public class PersonAssembler implements PersonAssemblerInt{

    @Override
    public PersonDTO modelToDTO(Person person) {
        return new PersonDTO(person.getId(), person.getAge(), person.getName(), person.getAddress(), person.getPhoneNumber());
    }

    @Override
    public Person dtoToModel(PersonDTO personDTO) {
        return new Person(personDTO.getId(), personDTO.getAge(), personDTO.getName(), personDTO.getAddress(), personDTO.getPhoneNumber());
    }
}

