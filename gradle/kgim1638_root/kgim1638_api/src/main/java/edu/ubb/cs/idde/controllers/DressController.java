package edu.ubb.cs.idde.controllers;

import edu.ubb.cs.idde.DressDTO;
import edu.ubb.cs.idde.assemblers.DressAssembler;
import edu.ubb.cs.idde.models.Dress;
import edu.ubb.cs.idde.services.DressService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "https://pls-dot-wired-rhino-225308.appspot.com/")
//@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/api")
public class DressController {

    @Autowired
    private DressService dressService;

    @Autowired
    private DressAssembler dressAssembler;

    @RequestMapping(value = "/dresses")
    public ResponseEntity<List<DressDTO>> findAllDresss() throws Exception {
        try {
            List<Dress> dresses = dressService.findAllDresss();
            List<DressDTO> dressDtos =  new ArrayList<DressDTO>();

            for(Dress dress:dresses)
            {
                dressDtos.add(dressAssembler.modelToDTO(dress));
            }

            return ResponseEntity.ok(dressDtos);
        } catch (Exception e) {
            throw new Exception("findAllDresss failed");
        }
    }

    @GetMapping("/dresses/{id}")
    public ResponseEntity<DressDTO> findDressById(@PathVariable long id) throws Exception {
        DressDTO dressDTO;
        try {
            try {
                dressDTO = dressAssembler.modelToDTO(dressService.findDressById(id));
                return ResponseEntity.ok(dressDTO);
            } catch (NotFoundException e) {
                throw new Exception("dress not found");
            }
        } catch (Exception e) {
            throw new Exception("getDressById failed");
        }
    }

    @PostMapping("/dresses")
    public ResponseEntity<String> createDress(@RequestBody DressDTO dressDTO) throws Exception {
        try {
            dressService.saveDress(dressAssembler.dtoToModel(dressDTO));
            return ResponseEntity.ok("dress successful created");
        } catch (Exception e) {
            throw new Exception("create dress failed");
        }
    }

    @PutMapping("/dresses/{id}")
    public ResponseEntity<Object> updateDress(@RequestBody DressDTO dressDTO, @PathVariable long id) throws  Exception {
        try {
            dressService.updateDress(dressAssembler.dtoToModel(dressDTO), id);
            return ResponseEntity.ok("dress successful updated");
        } catch(NotFoundException e){
            throw new Exception("dress not found");
        }
        catch (Exception e) {
            throw new Exception("createDress failed");
        }
    }

    @DeleteMapping("/dresses/{id}")
    public ResponseEntity<String> deleteDress(@PathVariable long id) throws Exception {
        try {
            dressService.deleteDressById(id);
            return ResponseEntity.ok("dress successful deleted");
        } catch (Exception e) {
            throw new Exception("deleteDressById failed");
        }
    }
}
