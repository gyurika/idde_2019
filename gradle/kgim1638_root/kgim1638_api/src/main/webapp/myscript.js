$(document).ready(function(){
    $.get("https://pls-dot-wired-rhino-225308.appspot.com/api/books",
        function (data, status) {
            console.log(data[0].title);

            var table = document.getElementById("myTable");

            
            data.forEach( book => {
                var row = table.insertRow(0);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                
                cell1.innerHTML = book.title;
                cell2.innerHTML = book.author;
            });
        }
    );
})