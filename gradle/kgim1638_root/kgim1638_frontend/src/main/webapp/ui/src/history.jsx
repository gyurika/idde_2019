import {createBrowserHistory} from 'history';

function createClientHistory() {
    if (typeof document === 'undefined') return;
    return createBrowserHistory();
}

export const history = createClientHistory();

export function navigateTo(path) {
    history.push(path)
}