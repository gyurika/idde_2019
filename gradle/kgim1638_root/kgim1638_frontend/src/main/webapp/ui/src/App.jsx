import React, { Component } from 'react';
import {Route, Router} from 'react-router';
import './style.scss';
import {AppRoutes} from "./AppRoutes";
import {history} from "./history";

class App extends Component {
    render() {
        return (
            <Router history={history}>
                <Route path="/" render={(props) => {
                    document.body.className = `router-${props.location.pathname.replace(/\//g, '-') || 'root'}`;
                    return (<AppRoutes {...props}/>);
                }}/>
            </Router>
        );
    }
}

export default App;
