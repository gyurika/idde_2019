package edu.ubb.cs.idde;

public abstract class BaseDTO {
    
    protected Long id;

    public BaseDTO(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
