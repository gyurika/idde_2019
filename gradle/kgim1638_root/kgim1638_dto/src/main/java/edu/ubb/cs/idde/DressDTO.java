package edu.ubb.cs.idde;

public class DressDTO extends BaseDTO{

    private String colour;
    private int size;

    public DressDTO() {}

    public DressDTO(Long id, String colour, int size) {
        this.id = id;
        this.colour = colour;
        this.size = size;

    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}

