package edu.ubb.cs.idde;

public class PersonDTO extends BaseDTO{

    private int age;
    private String name;
    private String address;
    private String phoneNumber;

    public PersonDTO(){}

    public PersonDTO(Long id, int age, String name, String address, String phoneNumber)
    {
        this.id = id;
        this.age = age;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
