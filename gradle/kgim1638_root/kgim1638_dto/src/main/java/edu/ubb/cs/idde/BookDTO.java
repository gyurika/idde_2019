package edu.ubb.cs.idde;

public class BookDTO extends BaseDTO{

    private String title;
    private String author;

    public BookDTO() {}

    public BookDTO(Long id, String title, String author) {
        this.id = id;
        this.title = title;
        this.author = author;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

}
