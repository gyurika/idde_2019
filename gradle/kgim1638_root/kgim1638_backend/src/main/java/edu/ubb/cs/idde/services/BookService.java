package edu.ubb.cs.idde.services;

import edu.ubb.cs.idde.models.Book;
import edu.ubb.cs.idde.repositories.BookRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<Book> findAllBooks() throws Exception {
        try {
            return (List<Book>)bookRepository.findAll();
        } catch (DataAccessException e) {
            throw new Exception("findAllBooks failed");
        }
    }

    public Book findBookById(long id) throws Exception{
        try {
            Optional<Book> book = bookRepository.findById(id);

            if (!book.isPresent())
                throw new NotFoundException("not found id-" + id);

            return book.get();
        }catch(DataAccessException e){
            throw new Exception("findBookById failed");
        }
    }

    public void saveBook(Book book) throws Exception{
        try {
            bookRepository.save(book);
        } catch (DataAccessException e) {
            throw new Exception("saveBook failed");
        }
    }

    public void updateBook(Book book, long id) throws Exception{
        try {
            Optional<Book> bookOptional = bookRepository.findById(id);

            if (!bookOptional.isPresent())
                throw new NotFoundException("not found id-" + id);

            book.setId(id);

            bookRepository.save(book);

        } catch(DataAccessException e) {
            throw new Exception("updateBook failed");
        }
    }

    public void deleteBookById(long id) throws Exception{
        try {
            bookRepository.deleteById(id);
        } catch (DataAccessException e) {
            throw new Exception("deleteBookById failed");
        }
    }
}
