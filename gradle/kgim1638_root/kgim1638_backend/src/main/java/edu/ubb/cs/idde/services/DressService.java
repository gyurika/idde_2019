package edu.ubb.cs.idde.services;

import edu.ubb.cs.idde.models.Dress;
import edu.ubb.cs.idde.repositories.DressRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class DressService {

    @Autowired
    private DressRepository dressRepository;

    public List<Dress> findAllDresss() throws Exception {
        try {
            return (List<Dress>)dressRepository.findAll();
        } catch (DataAccessException e) {
            throw new Exception("findAllDresss failed");
        }
    }

    public Dress findDressById(long id) throws Exception{
        try {
            Optional<Dress> dress = dressRepository.findById(id);

            if (!dress.isPresent())
                throw new NotFoundException("not found id-" + id);

            return dress.get();
        }catch(DataAccessException e){
            throw new Exception("findDressById failed");
        }
    }

    public void saveDress(Dress dress) throws Exception{
        try {
            dressRepository.save(dress);
        } catch (DataAccessException e) {
            throw new Exception("saveDress failed");
        }
    }

    public void updateDress(Dress dress, long id) throws Exception{
        try {
            Optional<Dress> dressOptional = dressRepository.findById(id);

            if (!dressOptional.isPresent())
                throw new NotFoundException("not found id-" + id);

            dress.setId(id);

            dressRepository.save(dress);

        } catch(DataAccessException e) {
            throw new Exception("updateDress failed");
        }
    }

    public void deleteDressById(long id) throws Exception{
        try {
            dressRepository.deleteById(id);
        } catch (DataAccessException e) {
            throw new Exception("deleteDressById failed");
        }
    }
}
