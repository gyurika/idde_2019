package edu.ubb.cs.idde.services;

import edu.ubb.cs.idde.models.Person;
import edu.ubb.cs.idde.repositories.PersonRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public List<Person> findAllPersons() throws Exception {
        try {
            return (List<Person>)personRepository.findAll();
        } catch (DataAccessException e) {
            throw new Exception("findAllPersons failed");
        }
    }

    public Person findPersonById(long id) throws Exception{
        try {
            Optional<Person> person = personRepository.findById(id);

            if (!person.isPresent())
                throw new NotFoundException("not found id-" + id);

            return person.get();
        }catch(DataAccessException e){
            throw new Exception("findPersonById failed");
        }
    }

    public void savePerson(Person person) throws Exception{
        try {
            personRepository.save(person);
        } catch (DataAccessException e) {
            throw new Exception("savePerson failed");
        }
    }

    public void updatePerson(Person person, long id) throws Exception{
        try {
            Optional<Person> personOptional = personRepository.findById(id);

            if (!personOptional.isPresent())
                throw new NotFoundException("not found id-" + id);

            person.setId(id);

            personRepository.save(person);

        } catch(DataAccessException e) {
            throw new Exception("updatePerson failed");
        }
    }

    public void deletePersonById(long id) throws Exception{
        try {
            personRepository.deleteById(id);
        } catch (DataAccessException e) {
            throw new Exception("deletePersonById failed");
        }
    }
}
