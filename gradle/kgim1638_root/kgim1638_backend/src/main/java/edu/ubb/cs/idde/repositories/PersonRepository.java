package edu.ubb.cs.idde.repositories;

import edu.ubb.cs.idde.models.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long>{}
