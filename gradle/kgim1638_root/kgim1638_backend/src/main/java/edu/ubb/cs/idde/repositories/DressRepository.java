package edu.ubb.cs.idde.repositories;

import edu.ubb.cs.idde.models.Dress;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DressRepository extends CrudRepository<Dress, Long> {}
