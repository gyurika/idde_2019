package edu.ubb.cs.idde.models;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Table;

@Entity
@Table(name = "Dress")
public class Dress extends BaseEntity {

    @Column(name = "colour")
    private String colour;
    @Column(name = "size")
    private int size;

    public Dress(){}

    public Dress(Long id, String colour, int size)
    {
        this.id = id;
        this.colour = colour;
        this.size = size;
    }

    public String getColour() {
        return colour;
    }

    public int getSize() {
        return size;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
