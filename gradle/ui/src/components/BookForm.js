import React from "react";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";

export default class Form extends React.Component {
  state = {
    title: "",
    titleError: "",
    authorName: "",
    authorError: "",
  };

  change = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  validate = () => {
    let isError = false;
    const errors = {
      titleError: "",
      authorError: ""
    };

    if (this.state.title.length < 2) {
      isError = true;
      errors.titleError = "title needs to be atleast 2 characters long";
    }

    if (this.state.author.length < 2) {
        isError = true;
        errors.authorError = "author needs to be atleast 2 characters long";
    }

    this.setState({
      ...this.state,
      ...errors
    });

    return isError;
  };

  onSubmit = e => {
    e.preventDefault();
    const err = this.validate();
    if (!err) {
      this.props.onSubmit(this.state);
      // clear form
      this.setState({
        title: "",
        titleError: "",
        authorName: "",
        authorError: ""
      });
    }
  };

  render() {
    return (
      <form>
        <TextField
          name="title"
          hintText="Title"
          floatingLabelText="Title"
          value={this.state.title}
          onChange={e => this.change(e)}
          errorText={this.state.titleError}
          floatingLabelFixed
        />
        <br />
        <TextField
          name="author"
          hintText="Author"
          floatingLabelText="Author"
          value={this.state.author}
          onChange={e => this.change(e)}
          errorText={this.state.authorError}
          floatingLabelFixed
        />

        <RaisedButton label="Submit" onClick={e => this.onSubmit(e)} primary />
      </form>
    );
  }
}