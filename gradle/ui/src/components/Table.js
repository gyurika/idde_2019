import React from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import EditIcon from 'material-ui/svg-icons/image/edit'
import TrashIcon from 'material-ui/svg-icons/action/delete'

const row = (elm, ind, header, handleRemove) =>(
    <TableRow key={`tr-${ind}`} selectable = {false} >
        {
            header.map((nextElm, nextInd) =><TableRowColumn key={`trc-${nextInd}`}>{elm[nextElm.prop]}</TableRowColumn>)
        }
        <TableRowColumn>
          <EditIcon />
        </TableRowColumn>

        <TableRowColumn>
            <TrashIcon onClick={() => handleRemove(ind)} />
        </TableRowColumn>
    </TableRow>
);
export default ({data, header, handleRemove}) => 
  <Table>
    <TableHeader>
      <TableRow>
          {
            header.map((elm, ind) =><TableHeaderColumn key={`thc-${ind}`}>{elm.name}</TableHeaderColumn>)
          }
        <TableHeaderColumn />
        <TableHeaderColumn />
      </TableRow>
    </TableHeader>
    <TableBody>
        {data.map((elm, ind) => row(elm, ind, header, handleRemove))}
    </TableBody>
  </Table>

