import React from 'react';
import './Layout.scss';
import {Header} from "./Header";

export class Layout extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="layout">
                <Header/>
                <div className="layout-content">
                    {this.props.children}
                </div>
            </div>
        );
    }

}