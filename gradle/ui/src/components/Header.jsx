import React from 'react';
import './Header.scss';
import {navigateTo} from "../history";

export class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    onClickBook(){
        navigateTo("/books");
    }
    onClickPerson(){
        navigateTo("/persons");
    }
    onClickDress(){
        navigateTo("/dresses");
    }
    render() {
        return (
            <div className="header">
                <div className="container">
                    <div className="button-container">
                        <div className="">
                            <button className="header-button" onClick={this.onClickBook}>Books</button>
                            <button className="header-button" onClick={this.onClickPerson}>Persons</button>
                            <button className="header-button" onClick={this.onClickDress}>Dresses</button>
                        </div>
                        <div className="header-title">
                            Have Fun
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}