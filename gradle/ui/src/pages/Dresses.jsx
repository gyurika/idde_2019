import React from 'react';
import Table from "../components/Table"
import { MuiThemeProvider } from 'material-ui/styles';
var axios = require('axios');

export class Dresses extends React.Component{
    constructor(props) {
        super(props);
        this.state ={
            dresses: []
        }
    }

    componentDidMount(){
        axios.get('http://localhost:8080/api/dresses')
        .then(response =>{ 
            this.setState({
                dresses : response.data
            })
        })
        .catch(err =>{ alert("error ", err.response.data);})
    }


    render() {
        const dresses = this.state.dresses;
        const dressTable = dresses.length ? (
        <MuiThemeProvider>
            <div className="Dress">
                <Table header={[
                    {
                        name: 'Id',
                        prop: 'id'
                    },
                    {
                        name: 'Colour',
                        prop: 'colour'
                    },
                    {
                        name: 'Size',
                        prop: 'size'
                    }
                ]} 
                data = {dresses} />
            </div>
        </MuiThemeProvider>
        ) : ( <div className="center">No dress yet</div>)

        return (
            <div className="container">
            <h4 className="center"> Dress </h4>
            {dressTable}
            </div>
        );
    }
}