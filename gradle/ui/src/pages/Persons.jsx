import React from 'react';
import Table from "../components/Table"
import { MuiThemeProvider } from 'material-ui/styles';
var axios = require('axios');

export class Persons extends React.Component{
    constructor(props) {
        super(props);
        this.state ={
            persons: []
        }
    }

    componentDidMount(){
        axios.get('http://localhost:8080/api/persons')
        .then(response =>{ 
            this.setState({
                persons : response.data
            })
        })
        .catch(err =>{ alert("error ", err.response.data);})
    }


    render() {
        const persons = this.state.persons;
        const personTable = persons.length ? (
        <MuiThemeProvider>
            <div className="Person">
                <Table header={[
                    {
                        name: 'Id',
                        prop: 'id'
                    },
                    {
                        name: 'Age',
                        prop: 'age'
                    },
                    {
                        name: 'Address',
                        prop: 'address'
                    },
                    {
                        name: 'Phone number',
                        prop: 'phoneNumber'
                    }
                ]} 
                data = {persons} />
            </div>
        </MuiThemeProvider>
        ) : ( <div className="center">No person yet</div>)

        return (
            <div className="container">
            <h4 className="center"> Person </h4>
            {personTable}
            </div>
        );
    }
}