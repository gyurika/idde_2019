import React from 'react';
import Table from "../components/Table"
import BookForm from "../components/BookForm";
import { MuiThemeProvider } from 'material-ui/styles';
var axios = require('axios');

export class Books extends React.Component{
    constructor(props) {
        super(props);
        this.state ={
            books: []
        }
    }

    componentDidMount(){
        axios.get('http://localhost:8080/api/books')
        .then(response =>{ 
            this.setState({
                books : response.data
            })
        })
        .catch(err =>{ alert("error ", err.response.data);})
    }

    handleRemove = index => {
        var deletedId =  this.state.books[index].id;
        axios.delete('http://localhost:8080/api/books/' + deletedId)
        .then(response =>{ 
            console.log(response);
            this.setState(state => ({
                books: state.books.filter((row, i) => i !== index)
            }));
        })
        .catch(err =>{ alert("error ", err.response.data);})
    };

    handleAdd = book => {

        console.log(book.title);
        axios.post('http://localhost:8080/api/books/', {title : book.title, author : book.author})
        .then(response =>{ 
            console.log(response);
            this.setState({
                books: [...this.state.books, book]
            });
        })
        .catch(err =>{ alert("error ", err.response.data);})
    }


    render() {
        const books = this.state.books;
        const bookTable = books.length ? (
        <MuiThemeProvider>
            <div className="Book">

                <BookForm
                    onSubmit={this.handleAdd}
                />
                <Table 
                    handleRemove={this.handleRemove}
                    header={[
                        {
                            name: 'Id',
                            prop: 'id'
                        },
                        {
                            name: 'Title',
                            prop: 'title'
                        },
                        {
                            name: 'Author',
                            prop: 'author'
                        }
                    ]} 
                    data = {books}
                />
            </div>
        </MuiThemeProvider>
        ) : (
            <MuiThemeProvider>
                <BookForm
                    onSubmit={this.handleAdd}
                />
                <div className="center">No book yet</div>
            </MuiThemeProvider>
        )

        return (
            <div className="container">
            <h4 className="center"> Book </h4>
            {bookTable}
            </div>
        );
    }
}