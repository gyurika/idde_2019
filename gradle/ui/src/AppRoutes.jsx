import React from 'react';
import {Route} from 'react-router';
import {Home} from "./pages/Home";
import {Layout} from "./components/Layout"
import {Books} from "./pages/Books";
import {Dresses} from "./pages/Dresses";
import {Persons} from "./pages/Persons";

export class AppRoutes extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Layout {...this.props}>
                <Route exact path="/" component={Home}/>
                <Route exact path="/books" component={Books}/>
                <Route exact path="/persons" component={Persons}/>
                <Route exact path="/dresses" component={Dresses}/>
            </Layout>
        );
    }
}