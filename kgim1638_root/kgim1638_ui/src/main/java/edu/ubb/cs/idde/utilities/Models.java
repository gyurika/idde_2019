package edu.ubb.cs.idde.utilities;

public enum Models {
    person("person"),
    book("book"),
    dress("dress");

    private String model;

    Models(String model)
    {
        this.model = model;
    }

    public String getModel(){
        return model;
    }

}