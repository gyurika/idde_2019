package edu.ubb.cs.idde.utilities;

public class ViewConstant {

    public static final String APPLICATION_TITLE = "person";
    public static final String SHOW_BUTTON = "show";
    public static final String RESOURCE_BUNDLE = "languages";

}
