package edu.ubb.cs.idde;

import edu.ubb.cs.idde.test.Test;
import edu.ubb.cs.idde.views.ModelView;

public class App 
{
    public static void main( String[] args )
    {
        new Test();
        new ModelView();
    }
}
