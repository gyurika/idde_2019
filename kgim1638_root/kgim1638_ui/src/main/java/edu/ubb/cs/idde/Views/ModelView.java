package edu.ubb.cs.idde.views;
import edu.ubb.cs.idde.daos.DatabaseDAO;
import edu.ubb.cs.idde.daos.FactoryDAO;
import edu.ubb.cs.idde.models.Book;
import edu.ubb.cs.idde.models.Dress;
import edu.ubb.cs.idde.models.Person;
import edu.ubb.cs.idde.daos.PersonDao;
import edu.ubb.cs.idde.utilities.DataVector;
import edu.ubb.cs.idde.utilities.Languages;
import edu.ubb.cs.idde.utilities.Models;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import static edu.ubb.cs.idde.utilities.ViewConstant.*;
import static edu.ubb.cs.idde.utilities.ViewConstant.*;

public class ModelView extends JFrame{

    private JPanel modelJPanel = new JPanel();
    private JButton showJButton = new JButton();
    private JTable modelJTable = new JTable();
    private JComboBox<Languages> languageJComboBox = new JComboBox<Languages>(Languages.values());
    private JComboBox<Models> modelJComboBox = new JComboBox<Models>(Models.values());
    private DefaultTableModel tableModel = new DefaultTableModel();
    private String language;

    public ModelView()
    {
        JScrollPane modelJScrollPane;
        modelJPanel.setBounds(0, 0, 600, 350);
        modelJPanel.setLayout(null);

        modelJTable.setModel(tableModel);

        modelJScrollPane = new JScrollPane(modelJTable );
        modelJScrollPane.setBounds(5, 10, 570, 200);
        modelJScrollPane.setVisible(true);

        //showJButton.setVisible(true);
        //showJButton.setBounds(250, 220, 100, 40);
        modelJComboBox.setVisible(true);
        modelJComboBox.setBounds(250, 220, 100, 40);
        languageJComboBox.setVisible(true);
        languageJComboBox.setBounds(250, 260, 100, 40);
        //modelJPanel.add(showJButton);
        modelJPanel.add(modelJScrollPane);
        modelJPanel.add(languageJComboBox);
        modelJPanel.add(modelJComboBox);
        this.add(modelJPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        modelJPanel.setVisible(true);

        setVisible(true);
        setBounds(100, 100, 600, 350);
        actionListeners();
        languageJComboBox.setSelectedIndex(0);
        modelJComboBox.setSelectedIndex(0);
    }

    private void fillTable()
    {
        Object columnIdentifiers[] = getColumnIdentifiers();
        Object dataVector[][] = getDataVector();

        tableModel.setDataVector(dataVector, columnIdentifiers);
    }

    private Object[] getColumnIdentifiers()
    {
        Object columnIdentifiers[];
        ArrayList<String> fieldNames = new ArrayList<>();
        Field[] fields;

        if(modelJComboBox.getSelectedItem().toString().equals("person"))
        {
            fields = Person.class.getDeclaredFields();
        }
        else if (modelJComboBox.getSelectedItem().toString().equals("dress"))
        {
            fields = Dress.class.getDeclaredFields();
        }
        else
        {
            fields = Book.class.getDeclaredFields();
        }


        for(Field field : fields)
        {
            if(!field.getName().contains("id"))
                fieldNames.add(field.getName());
        }

        Collections.sort(fieldNames);
        columnIdentifiers =  new Object[fieldNames.size()];

        Locale.setDefault(new Locale(language));
        ResourceBundle localeLanguages = ResourceBundle.getBundle(RESOURCE_BUNDLE);

        int i = 0;
        for(String fieldName : fieldNames)
            columnIdentifiers[i++] = localeLanguages.getString(fieldName);
        System.out.println(modelJComboBox.getSelectedItem().toString());
        this.setTitle(localeLanguages.getString(modelJComboBox.getSelectedItem().toString()));

        return  columnIdentifiers;
    }

    private Object[][] getDataVector()
    {
        Object dataVector[][];
        if(modelJComboBox.getSelectedItem().toString().equals("person"))
        {
            dataVector =  new DataVector<Person>(FactoryDAO.getInstance().getPersonDao()).getDataVector();
        }
        else if (modelJComboBox.getSelectedItem().toString().equals("dress"))
        {
            dataVector =  new DataVector<Dress>(FactoryDAO.getInstance().getDressDao()).getDataVector();
        }
        else
        {
            dataVector =  new DataVector<Book>(FactoryDAO.getInstance().getBookDao()).getDataVector();
        }
        return dataVector;
    }

    private void actionListeners(){
        modelJComboBox.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        fillTable();
                    }
                }
        );

        languageJComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                language = Languages.valueOf(languageJComboBox.getSelectedItem().toString()).getLanguage();
                tableModel.setColumnIdentifiers(getColumnIdentifiers());
            }
        });
    }
}
