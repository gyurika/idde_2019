package edu.ubb.cs.idde.test;

import edu.ubb.cs.idde.daos.DatabaseDAO;
import edu.ubb.cs.idde.daos.FactoryDAO;
import edu.ubb.cs.idde.models.Book;

import java.util.ArrayList;

public class Test {
    public Test(){
        System.out.println("####################TEST########################");

        DatabaseDAO<Book> bookTable = FactoryDAO.getInstance().getBookDao();

        ArrayList<Book> books = bookTable.getAll();

        System.out.println("initially");

        for(Book element : books) {
            System.out.println(element.getAuthor() + " " + element.getTitle());
        }
        System.out.println("INSERT Book(Jancsika, Vitez)");
        bookTable.create(new Book("Jancsika", "Vitez"));


        System.out.println("UPDATE  Book(Jancsika, Vitez) => Book(Jancsika, Vitezke)");
        books.get(2).setAuthor("Vitezke");
        bookTable.update(books.get(2));

        ;
        System.out.println("READ FIRST ROW");
        System.out.println(bookTable.read(1).getAuthor() +" " + bookTable.read(1).getTitle());

        bookTable.read(1);
        System.out.println("DELETE FIRST ROW");
        //bookTable.delete(books.get(0));

        System.out.println("in the end");

        books = bookTable.getAll();
        for(Book element : books) {
            System.out.println(element.getAuthor() + " " + element.getTitle());
        }

    }
}
