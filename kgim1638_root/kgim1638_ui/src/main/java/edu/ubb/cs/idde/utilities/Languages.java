package edu.ubb.cs.idde.utilities;

public enum Languages {
    Hungarian("hu"),
    English("en"),
    Romanian("ro");

    private String language;

    Languages(String language)
    {
        this.language = language;
    }

    public String getLanguage(){
        return language;
    }

}
