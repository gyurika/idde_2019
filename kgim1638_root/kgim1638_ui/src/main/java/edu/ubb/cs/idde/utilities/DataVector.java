package edu.ubb.cs.idde.utilities;

import edu.ubb.cs.idde.daos.DatabaseDAO;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;

public class DataVector<T> {

    private Object dataVector[][];
    public DataVector(DatabaseDAO<T> databaseDAO){

        int i, j;
        i = 0;

        ArrayList<T> data = databaseDAO.getAll();
        Method[] methods = data.get(0).getClass().getDeclaredMethods();
        ArrayList<Method> getMethods =  new ArrayList<>();
        ArrayList<String> methodNames =  new ArrayList<>();

        for(Method method : methods)
        {
            if(!method.getName().contains("getId") && method.getName().contains("get"))
            {
                methodNames.add(method.getName());
            }
        }
        Collections.sort(methodNames);

        for(String methodName : methodNames)
        {
            for(Method method : methods)
            {
                if(method.getName().equals(methodName))
                {
                    getMethods.add(method);
                }
            }
        }

        dataVector = new Object[data.size()][getMethods.size()];

        for(T elem : data) {
            j = 0;
            for (Method getMethod : getMethods)
            {    
                try {
                    dataVector[i][j] = getMethod.invoke(elem);
                    j++;
                } catch (IllegalAccessException | InvocationTargetException e) {
                    System.out.println("can not show");
                }
            }

            i++;
        }

    }

    public Object[][] getDataVector(){
        return dataVector;
    }
}
