package edu.ubb.cs.idde.daos;

import edu.ubb.cs.idde.models.Book;
import edu.ubb.cs.idde.models.Dress;
import edu.ubb.cs.idde.models.Person;

public class JDBCFactoryDAO extends FactoryDAO{

    @Override
    public DatabaseDAO<Person> getPersonDao() {
        return new PersonDao();
    }

    @Override
    public DatabaseDAO<Book> getBookDao() {
        return new BookDao();
    }

    @Override
    public DatabaseDAO<Dress> getDressDao() {
        return new DressDao();
    }
}
