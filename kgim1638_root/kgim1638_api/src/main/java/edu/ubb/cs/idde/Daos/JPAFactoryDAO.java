package edu.ubb.cs.idde.daos;

import edu.ubb.cs.idde.models.Book;
import edu.ubb.cs.idde.models.Dress;
import edu.ubb.cs.idde.models.Person;

public class JPAFactoryDAO extends FactoryDAO {

    @Override
    public DatabaseDAO<Person> getPersonDao() {
        return new PersonJPADao();
    }

    @Override
    public DatabaseDAO<Book> getBookDao() {
        return new BookJPADao();
    }

    @Override
    public DatabaseDAO<Dress> getDressDao() {
        return new DressJPADao();
    }
}
