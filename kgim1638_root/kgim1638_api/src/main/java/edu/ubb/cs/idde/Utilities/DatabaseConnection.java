package edu.ubb.cs.idde.utilities;

import edu.ubb.cs.idde.exceptions.DatabaseException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import static edu.ubb.cs.idde.utilities.DatabaseConfiguration.*;

public class DatabaseConnection {
    private Connection connect = null;

    public Connection openConnection()throws DatabaseException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(URL+"?user="+USER+"&password="+PASS+"&useSSL=false");
        } catch (ClassNotFoundException exception ) {
            throw new DatabaseException(exception);
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        return connect;
    }
}
