package edu.ubb.cs.idde.exceptions;

public class DatabaseException extends RuntimeException {

    public DatabaseException(final String message)
    {
        super(message);
    }

    public DatabaseException(final Throwable cause)
    {
        super(cause);
    }

    public DatabaseException(final String message, final Throwable cause)
    {
        super(message, cause);
    }
}
