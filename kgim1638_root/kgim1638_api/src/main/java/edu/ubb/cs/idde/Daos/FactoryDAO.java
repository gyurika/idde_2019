package edu.ubb.cs.idde.daos;

import edu.ubb.cs.idde.models.Book;
import edu.ubb.cs.idde.models.Dress;
import edu.ubb.cs.idde.models.Person;

public abstract class FactoryDAO {
    private static FactoryDAO factoryDAO = null;
    public static FactoryDAO getInstance(){
        if(factoryDAO == null)
            factoryDAO = new JPAFactoryDAO();
        return factoryDAO;
    }

    public abstract DatabaseDAO<Person> getPersonDao();
    public abstract DatabaseDAO<Book> getBookDao();
    public abstract DatabaseDAO<Dress> getDressDao();
}
