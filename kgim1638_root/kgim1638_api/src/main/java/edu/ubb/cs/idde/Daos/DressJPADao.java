package edu.ubb.cs.idde.daos;

import edu.ubb.cs.idde.exceptions.DatabaseException;
import edu.ubb.cs.idde.models.Dress;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;


public class DressJPADao implements DatabaseDAO<Dress> {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    public DressJPADao(){
        entityManagerFactory = Persistence.createEntityManagerFactory("ModelJPA");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public ArrayList<Dress> getAll() throws DatabaseException {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Dress> criteriaQuery = criteriaBuilder.createQuery(Dress.class);
        CriteriaQuery<Dress> all = criteriaQuery.select(criteriaQuery.from(Dress.class));
        TypedQuery<Dress> allQuery = entityManager.createQuery(all);

        ArrayList<Dress> dress = new ArrayList<>();
        for (Dress element : allQuery.getResultList())
            dress.add(element);

        return dress;
    }

    @Override
    public void create(Dress dress) {
        entityManager.getTransaction().begin();
        try {
            entityManager.persist(dress);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

    @Override
    public Dress read(int id) throws DatabaseException {
        try{
            return entityManager.find(Dress.class, id);
        } catch(Exception e){
            return null;
        }
    }

    @Override
    public void update(Dress newDress) {
        entityManager.getTransaction().begin();
        try {
            Dress dress = entityManager.find(Dress.class, newDress.getId());
            dress.setSize(newDress.getSize());
            dress.setColour(newDress.getColour());
            entityManager.getTransaction().commit();
        } catch (IllegalArgumentException e) {
            entityManager.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Dress dress) {
        entityManager.getTransaction().begin();
        try {
            entityManager.remove(entityManager.find(Dress.class, dress.getId()));
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

}
