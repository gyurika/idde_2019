package edu.ubb.cs.idde.utilities;

public class DatabaseQueries {
    public static final String GET_ALL_PERSON_QUERY = "select * from Person";
    public static final String CREATE_PERSON_QUERY = "INSERT INTO Person( name, age, address, phoneNumber ) VALUES ";
    public static final String READ_PERSON_QUERY = "SELECT * FROM Person WHERE id =";
    public static final String UPDATE_PERSON_QUERY = "UPDATE Person SET ";
    public static final String DELETE_PERSON_QUERY = "DELETE FROM Person WHERE id =";
    public static final String GET_ALL_DRESS_QUERY = "select * from Dress";
    public static final String CREATE_DRESS_QUERY = "INSERT INTO Dress(colour, size ) VALUES ";
    public static final String READ_DRESS_QUERY = "SELECT * FROM Dress WHERE id =";
    public static final String UPDATE_DRESS_QUERY = "UPDATE Dress SET ";
    public static final String DELETE_DRESS_QUERY = "DELETE FROM Dress WHERE id =";
    public static final String GET_ALL_BOOK_QUERY = "select * from Book";
    public static final String CREATE_BOOK_QUERY = "INSERT INTO Book( title, author ) VALUES ";
    public static final String READ_BOOK_QUERY = "SELECT * FROM Book WHERE id =";
    public static final String UPDATE_BOOK_QUERY = "UPDATE Book SET ";
    public static final String DELETE_BOOK_QUERY = "DELETE FROM Book WHERE id =";
}
