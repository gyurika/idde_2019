package edu.ubb.cs.idde.daos;

import edu.ubb.cs.idde.exceptions.DatabaseException;
import edu.ubb.cs.idde.models.Person;
import edu.ubb.cs.idde.utilities.DatabaseConnection;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import static edu.ubb.cs.idde.utilities.DatabaseQueries.*;

public class PersonDao implements DatabaseDAO<Person> {
    private DatabaseConnection connectionDatabase;
    private Connection connect;
    private ResultSet resultSet;
    private Statement statement;

    public PersonDao()
    {
        connectionDatabase = new DatabaseConnection();
        connect = null;
        resultSet = null;
        statement = null;
    }

    public ArrayList<Person> getAll() throws DatabaseException {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL_PERSON_QUERY );

            return getAllPersons(resultSet);

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not close database connection");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public void create(Person person) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(CREATE_PERSON_QUERY + "("+ person.getName() +", "+ person.getAge() +", " + person.getAddress() +", " + person.getPhoneNumber() + ")");
            //return person;

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not create row");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public Person read(int id) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(READ_PERSON_QUERY + id);
            Field[] fields = Person.class.getDeclaredFields();

            return new Person(resultSet.getInt(fields[0].getName()), resultSet.getInt(fields[1].getName()), resultSet.getString(fields[2].getName()), resultSet.getString(fields[3].getName()), resultSet.getString(fields[4].getName()));


        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not read row");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public void update(Person person) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(UPDATE_PERSON_QUERY + "name = " + person.getName()+ ", age = " + person.getAge() +", address = " + person.getAddress() + ", phoneNumber =" + person.getPhoneNumber() + " WHERE id =" + person.getId() + ";");
            //return person;

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not update");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public void delete(Person person) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(DELETE_PERSON_QUERY + person.getId());
            //return person;

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not delete row");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public ArrayList<Person> getAllPersons(ResultSet result) throws SQLException
    {
        ArrayList<Person> persons = new ArrayList();
        Field[] fields = Person.class.getDeclaredFields();

        while(result.next())
        {
            persons.add(new Person(result.getInt(fields[0].getName()), result.getInt(fields[1].getName()), result.getString(fields[2].getName()), result.getString(fields[3].getName()), result.getString(fields[4].getName())));
        }

        return persons;
    }
}
