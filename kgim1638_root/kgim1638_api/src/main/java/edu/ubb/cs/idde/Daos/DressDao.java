package edu.ubb.cs.idde.daos;

import edu.ubb.cs.idde.exceptions.DatabaseException;
import edu.ubb.cs.idde.models.Dress;
import edu.ubb.cs.idde.utilities.DatabaseConnection;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import static edu.ubb.cs.idde.utilities.DatabaseQueries.*;

public class DressDao implements DatabaseDAO<Dress> {
    private DatabaseConnection connectionDatabase;
    private Connection connect;
    private ResultSet resultSet;
    private Statement statement;

    public DressDao()
    {
        connectionDatabase = new DatabaseConnection();
        connect = null;
        resultSet = null;
        statement = null;
    }

    public ArrayList<Dress> getAll() throws DatabaseException {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL_DRESS_QUERY);

            return getAllDress(resultSet);

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not close database connection");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public void create(Dress dress) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(CREATE_DRESS_QUERY + "("+ dress.getColour() +", "+ dress.getSize() + ")");
            //return dress;

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not create row");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public Dress read(int id) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(READ_DRESS_QUERY + id);
            Field[] fields = Dress.class.getDeclaredFields();

            return new Dress(resultSet.getInt(fields[0].getName()), resultSet.getString(fields[1].getName()), resultSet.getInt(fields[2].getName()));

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not read");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public void update(Dress dress) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(UPDATE_DRESS_QUERY + "colour = " + dress.getColour()+ ", size = " + dress.getSize() + " WHERE id =" + dress.getId() + ";");
            //return dress;

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not update row");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public void delete(Dress dress) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(DELETE_DRESS_QUERY + dress.getId());
            //return dress;

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not delete row");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public ArrayList<Dress> getAllDress(ResultSet result) throws SQLException
    {
        ArrayList<Dress> dress = new ArrayList();
        Field[] fields = Dress.class.getDeclaredFields();

        while(result.next())
        {
            dress.add(new Dress(result.getInt(fields[0].getName()), result.getString(fields[2].getName()), result.getInt(fields[1].getName())));
        }

        return dress;
    }


}
