package edu.ubb.cs.idde.daos;

import edu.ubb.cs.idde.exceptions.DatabaseException;

import java.util.ArrayList;

public interface DatabaseDAO<T> {
    ArrayList<T> getAll() throws DatabaseException;
    void create(T data) throws DatabaseException;
    T read(int data) throws DatabaseException;
    void update(T data) throws DatabaseException;
    void delete(T data) throws DatabaseException;
}
