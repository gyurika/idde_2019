package edu.ubb.cs.idde.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Dress")
public class Dress{
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "colour")
    private String colour;
    @Column(name = "size")
    private int size;

    public Dress(){}
    public Dress(int id, String colour, int size)
    {
        this.id = id;
        this.colour = colour;
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public String getColour() {
        return colour;
    }

    public int getSize() {
        return size;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setSize(int size) {
        this.size = size;
    }
}