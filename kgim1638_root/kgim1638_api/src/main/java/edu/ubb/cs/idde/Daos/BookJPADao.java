package edu.ubb.cs.idde.daos;

import edu.ubb.cs.idde.exceptions.DatabaseException;
import edu.ubb.cs.idde.models.Book;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;


public class BookJPADao implements DatabaseDAO<Book> {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    public BookJPADao() {
        entityManagerFactory = Persistence.createEntityManagerFactory("ModelJPA");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public ArrayList<Book> getAll() throws DatabaseException {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> criteriaQuery = criteriaBuilder.createQuery(Book.class);
        CriteriaQuery<Book> all = criteriaQuery.select(criteriaQuery.from(Book.class));
        TypedQuery<Book> allQuery = entityManager.createQuery(all);

        ArrayList<Book> books = new ArrayList<>();
        for (Book element : allQuery.getResultList())
            books.add(element);

        return books;
    }

    @Override
    public void create(Book book) {
        entityManager.getTransaction().begin();
        try {
            entityManager.persist(book);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

    @Override
    public Book read(int id) throws DatabaseException {
        try{
            return entityManager.find(Book.class, id);
        } catch(Exception e){
            return null;
        }
    }

    @Override
    public void update(Book newBook) {
        entityManager.getTransaction().begin();
        try {
            Book book = entityManager.find(Book.class, newBook.getId());
            book.setTitle(newBook.getTitle());
            book.setAuthor(newBook.getAuthor());
            entityManager.getTransaction().commit();
        } catch (IllegalArgumentException e) {
            entityManager.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Book book) {
        entityManager.getTransaction().begin();
        try {
            entityManager.remove(entityManager.find(Book.class, book.getId()));
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

}
