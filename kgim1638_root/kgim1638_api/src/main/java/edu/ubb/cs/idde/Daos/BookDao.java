package edu.ubb.cs.idde.daos;

import edu.ubb.cs.idde.exceptions.DatabaseException;
import edu.ubb.cs.idde.models.Book;
import edu.ubb.cs.idde.utilities.DatabaseConnection;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import static edu.ubb.cs.idde.utilities.DatabaseQueries.*;

public class BookDao implements DatabaseDAO<Book> {
    private DatabaseConnection connectionDatabase;
    private Connection connect;
    private ResultSet resultSet;
    private Statement statement;

    public BookDao()
    {
        connectionDatabase = new DatabaseConnection();
        connect = null;
        resultSet = null;
        statement = null;
    }

    public ArrayList<Book> getAll() throws DatabaseException {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL_BOOK_QUERY);

            return getAllBooks(resultSet);

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not close database connection");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public void create(Book book) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(CREATE_BOOK_QUERY + "("+ book.getTitle() +", "+ book.getAuthor()+ ")");
            //return book;

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not create row");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public Book read(int id) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(READ_BOOK_QUERY + id);
            Field[] fields = Book.class.getDeclaredFields();

            return new Book(resultSet.getInt(fields[0].getName()), resultSet.getString(fields[2].getName()), resultSet.getString(fields[1].getName()));


        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not read row");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public void update(Book book) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(UPDATE_BOOK_QUERY + "title = " + book.getTitle()+ ", author = " + book.getAuthor() +" WHERE id =" + book.getId() + ";");
            //return book;

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not update row");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public void delete(Book book) throws DatabaseException
    {
        try{
            connect = connectionDatabase.openConnection();
            statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(DELETE_BOOK_QUERY + book.getId());

        } catch (DatabaseException exception) {
            throw  new DatabaseException("can not delete row");
        }catch (SQLException exception)
        {
            throw new DatabaseException(exception);
        }
        finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (connect != null) {
                    connect.close();
                }
            } catch (DatabaseException exception) {
                throw  new DatabaseException("can not close database connection");
            }catch (SQLException exception)
            {
                throw new DatabaseException(exception);
            }
        }
    }

    public ArrayList<Book> getAllBooks(ResultSet result) throws SQLException
    {
        ArrayList<Book> books = new ArrayList();
        Field[] fields = Book.class.getDeclaredFields();

        while(result.next())
        {
            books.add(new Book(result.getInt(fields[0].getName()), result.getString(fields[2].getName()), result.getString(fields[1].getName())));
        }

        return books;
    }
}
