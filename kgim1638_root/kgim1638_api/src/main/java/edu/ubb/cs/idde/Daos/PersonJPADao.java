package edu.ubb.cs.idde.daos;

import edu.ubb.cs.idde.exceptions.DatabaseException;
import edu.ubb.cs.idde.models.Person;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;


public class PersonJPADao implements DatabaseDAO<Person> {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    public PersonJPADao() {
        entityManagerFactory = Persistence.createEntityManagerFactory("ModelJPA");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public ArrayList<Person> getAll() throws DatabaseException {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Person> criteriaQuery = criteriaBuilder.createQuery(Person.class);
        CriteriaQuery<Person> all = criteriaQuery.select(criteriaQuery.from(Person.class));
        TypedQuery<Person> allQuery = entityManager.createQuery(all);

        ArrayList<Person> persons = new ArrayList<>();
        for (Person element : allQuery.getResultList())
            persons.add(element);

        return persons;
    }

    @Override
    public void create(Person person) {
        entityManager.getTransaction().begin();
        try {
            entityManager.persist(person);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

    @Override
    public Person read(int id) throws DatabaseException {
        try{
            return entityManager.find(Person.class, id);
        } catch(Exception e){
            return null;
        }
    }

    @Override
    public void update(Person newPerson) {
        entityManager.getTransaction().begin();
        try {
            Person person = entityManager.find(Person.class, newPerson.getId());
            person.setAge(newPerson.getAge());
            person.setName(newPerson.getName());
            person.setAddress(newPerson.getAddress());
            person.setPhoneNumber(newPerson.getPhoneNumber());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Person person) {
        entityManager.getTransaction().begin();
        try {
            entityManager.remove(entityManager.find(Person.class, person.getId()));
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
    }
}
